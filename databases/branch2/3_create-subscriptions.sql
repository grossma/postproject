CREATE SUBSCRIPTION employee_sub_branch2 CONNECTION 'dbname=postamp host=main-office user=cp password=password' PUBLICATION employee_pub_main;
CREATE SUBSCRIPTION delivery_sub_branch2 CONNECTION 'dbname=postamp host=main-office user=cp password=password' PUBLICATION delivery_pub_main;
CREATE SUBSCRIPTION subscribe_sub_branch2 CONNECTION 'dbname=postamp host=main-office user=cp password=password' PUBLICATION subscribe_pub_main;
CREATE SUBSCRIPTION subscriber_sub_branch2 CONNECTION 'dbname=postamp host=main-office user=cp password=password' PUBLICATION subscriber_pub_main;
CREATE SUBSCRIPTION edition_sub_branch2 CONNECTION 'dbname=postamp host=main-office user=cp password=password' PUBLICATION edition_pub_main;
CREATE SUBSCRIPTION admission_sub_branch2 CONNECTION 'dbname=postamp host=main-office user=cp password=password' PUBLICATION admission_pub_main;