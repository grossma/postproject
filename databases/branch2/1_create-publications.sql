CREATE PUBLICATION employee_pub_branch2 FOR TABLE employee WITH (publish ='insert,update,delete');
CREATE PUBLICATION delivery_pub_branch2 FOR TABLE delivery WITH (publish ='insert,update,delete');
CREATE PUBLICATION subscribe_pub_branch2 FOR TABLE subscribe WITH (publish ='insert,update,delete');
CREATE PUBLICATION subscriber_pub_branch2 FOR TABLE subscriber WITH (publish ='insert,update,delete');
CREATE PUBLICATION edition_pub_branch2 FOR TABLE edition WITH (publish ='insert,update,delete');
CREATE PUBLICATION admission_pub_branch2 FOR TABLE admission WITH (publish ='insert,update,delete');
