CREATE PUBLICATION employee_pub_main FOR TABLE employee WITH (publish ='update,delete');
CREATE PUBLICATION delivery_pub_main FOR TABLE delivery WITH (publish ='update,delete');
CREATE PUBLICATION subscribe_pub_main FOR TABLE subscribe WITH (publish ='update,delete');
CREATE PUBLICATION subscriber_pub_main FOR TABLE subscriber WITH (publish ='update,delete');
CREATE PUBLICATION edition_pub_main FOR TABLE edition WITH (publish ='insert,update,delete');
CREATE PUBLICATION admission_pub_main FOR TABLE admission WITH (publish ='update,delete');
