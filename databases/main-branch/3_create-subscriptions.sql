CREATE SUBSCRIPTION employee_sub_branch1 CONNECTION 'dbname=postamp host=branch-one user=branch1 password=password' PUBLICATION employee_pub_branch1;
CREATE SUBSCRIPTION delivery_sub_branch1 CONNECTION 'dbname=postamp host=branch-one user=branch1 password=password' PUBLICATION delivery_pub_branch1;
CREATE SUBSCRIPTION subscribe_sub_branch1 CONNECTION 'dbname=postamp host=branch-one user=branch1 password=password' PUBLICATION subscribe_pub_branch1;
CREATE SUBSCRIPTION subscriber_sub_branch1 CONNECTION 'dbname=postamp host=branch-one user=branch1 password=password' PUBLICATION subscriber_pub_branch1;
CREATE SUBSCRIPTION edition_sub_branch1 CONNECTION 'dbname=postamp host=branch-one user=branch1 password=password' PUBLICATION edition_pub_branch1;
CREATE SUBSCRIPTION admission_sub_branch1 CONNECTION 'dbname=postamp host=branch-one user=branch1 password=password' PUBLICATION admission_pub_branch1;

CREATE SUBSCRIPTION employee_sub_branch2 CONNECTION 'dbname=postamp host=branch-two user=branch2 password=password' PUBLICATION employee_pub_branch2;
CREATE SUBSCRIPTION delivery_sub_branch2 CONNECTION 'dbname=postamp host=branch-two user=branch2 password=password' PUBLICATION delivery_pub_branch2;
CREATE SUBSCRIPTION subscribe_sub_branch2 CONNECTION 'dbname=postamp host=branch-two user=branch2 password=password' PUBLICATION subscribe_pub_branch2;
CREATE SUBSCRIPTION subscriber_sub_branch2 CONNECTION 'dbname=postamp host=branch-two user=branch2 password=password' PUBLICATION subscriber_pub_branch2;
CREATE SUBSCRIPTION edition_sub_branch2 CONNECTION 'dbname=postamp host=branch-two user=branch2 password=password' PUBLICATION edition_pub_branch2;
CREATE SUBSCRIPTION admission_sub_branch2 CONNECTION 'dbname=postamp host=branch-two user=branch2 password=password' PUBLICATION admission_pub_branch2;
