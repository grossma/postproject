CREATE TABLE IF NOT EXISTS subscriber
(
    id          serial,-- primary key,
    fio         varchar(256),
    address     text,
    phone       varchar(64),
    branch_code varchar(32),
    PRIMARY KEY (id, branch_code)
);
CREATE TABLE IF NOT EXISTS edition
(
    id               serial primary key,
    name             varchar(256),
    periodicity      varchar(256),
    price            real,
    publishing_house text

);
CREATE TABLE IF NOT EXISTS subscribe
(
    id            serial, --primary key,
    subscriber_id integer,
    date          timestamp,
    price         real,
    count         integer,
    edition_id    integer,
    branch_code   varchar(32),
    PRIMARY KEY (id, branch_code)

);
CREATE TABLE IF NOT EXISTS employee
(
    id          serial, -- primary key,
    fio         varchar(256),
    branch_code varchar(32),
    PRIMARY KEY (id, branch_code)
);
CREATE TABLE IF NOT EXISTS admission
(
    id          serial,-- primary key,
    date        timestamp,
    count       integer,
    edition_id  integer,
    branch_code varchar(32),
    PRIMARY KEY (id, branch_code)

);
CREATE TABLE IF NOT EXISTS delivery
(
    id              serial,-- primary key,
    subscription_id integer not null,
    count           integer not null,
    employee_id     integer not null,
    branch_code     varchar(32),
    PRIMARY KEY (id, branch_code)
);
-- CREATE TABLE IF NOT EXISTS user
-- (
--     id       serial primary key,
--     login    varchar(64),
--     password varchar(256)
-- );
--

