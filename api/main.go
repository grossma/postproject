package main

import (
	"log"
	"os"
	"postproject/db"
	"postproject/http"
)

func main() {
	server := http.NewServer()
	pgConfig := postgres.PgStore{}
	if err := pgConfig.Open(); err != nil {
		log.Fatal(err)
	}
	defer pgConfig.Close()
	handler := http.NewHandler(pgConfig.Db)

	server.Handler = handler
	if err := server.Open(); err != nil {
		log.Fatal(err)
	}
	defer func() {
		if err := server.Close(); err != nil {
			log.Printf("can't close server: %s\n", err.Error())
		}
	}()
	c := make(chan os.Signal, 1)
	_ = <-c
}
