package http

import (
	"encoding/json"
	"github.com/gorilla/sessions"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
)

var (
	// key must be 16, 24 or 32 bytes long (AES-128, AES-192 or AES-256)
	key   = []byte(os.Getenv("auth_secret_key"))
	store = sessions.NewCookieStore(key)
)

type User struct {
	ID       int    `json:"id"`
	Login    string `json:"login"`
	Password string `json:"password"`
}

func login(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	//Get received params
	var authData struct {
		Login    string `schema:"login"`
		Password string `schema:"password"`
	}
	err := json.NewDecoder(r.Body).Decode(&authData)
	//Get user from our system
	rows, err := db.Query("select id from user where login=$1 and password=$2", authData.Login, authData.Password)
	if err != nil {
		logrus.Errorf("can't get user: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var id int
	for rows.Next() {
		if err := rows.Scan(&id); err != nil {
			http.Error(w, err.Error(), http.StatusInternalServerError)
			return
		}
	}
	if id == 0 {
		http.Error(w, "forbidden", http.StatusForbidden)
		return
	}
	//Set auth
	session, _ := store.Get(r, "postproject-cookies")
	session.Values["authenticated"] = true
	if err := session.Save(r, w); err != nil {
		logrus.Error("can't save session: %s", err.Error())
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}
