package http

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	"github.com/julienschmidt/httprouter"
	"net/http"
)

var db *sqlx.DB

func NewHandler(pg *sqlx.DB) http.Handler {
	db = pg
	h := httprouter.New()
	h.GET("/subscribe/", getSubscribe)
	h.POST("/subscribe/", createSubscribe)
	h.PUT("/subscribe/:branchcode/:subID/", updateSubscribe)
	h.DELETE("/subscribe/:branchcode/:subID/", deleteSubscribe)
	h.GET("/subscribers/", getSubscriber)
	h.POST("/subscribers/", createSubscriber)
	h.PUT("/subscribers/:branchcode/:subID/", updateSubscriber)
	h.DELETE("/subscribers/:branchcode/:subID/", deleteSubscriber)
	h.GET("/edition/", getEdition)
	h.POST("/edition/", createEdition)
	h.PUT("/edition/:editID/", updateEdition)
	h.DELETE("/edition/:editID/", deleteEdition)
	h.GET("/employee/", getEmployee)
	h.POST("/employee/", createEmployee)
	h.PUT("/employee/:branchcode/:empID/", updateEmployee)
	h.DELETE("/employee/:branchcode/:empID/", deleteEmployee)
	h.GET("/admission/", getAdmission)
	h.POST("/admission/", createAdmission)
	h.PUT("/admission/:branchcode/:admID/", updateAdmission)
	h.DELETE("/admission/:branchcode/:admID/", deleteAdmission)
	h.GET("/delivery/", getDelivery)
	h.POST("/delivery/", createDelivery)
	h.PUT("/delivery/:branchcode/:dlvID/", updateDelivery)
	h.DELETE("/delivery/:branchcode/:dlvID/", deleteDelivery)

	h.POST("/auth/login", login)

	h.PanicHandler = func(w http.ResponseWriter, r *http.Request, i interface{}) {
		http.Error(w, fmt.Sprintf("%+v", i), http.StatusInternalServerError)
	}
	return h
}
