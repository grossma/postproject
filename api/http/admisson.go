package http

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
	"time"
)

type Admission struct {
	ID         int       `json:"id"`
	Date       time.Time `json:"date"`
	Count      int       `json:"count"`
	EditionID  int       `json:"edition_id"`
	BranchCode string    `json:"branch_code"`
}

func getAdmission(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	rows, err := db.Query(`select id, date, count, edition_id, branch_code from admission;`)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	admission := make([]Admission, 0)
	for rows.Next() {
		adm := Admission{}
		if err := rows.Scan(&adm.ID, &adm.Date, &adm.Count, &adm.EditionID, &adm.BranchCode); err != nil {
			logrus.Error(err.Error())
			continue
		}
		admission = append(admission, adm)
	}
	if err := json.NewEncoder(w).Encode(admission); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func createAdmission(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var adm Admission
	if err := json.NewDecoder(r.Body).Decode(&adm); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err := db.Query(`insert into admission (date, count, edition_id, branch_code) values ($1, $2, $3, $4);`, adm.Date, adm.Count, adm.EditionID, os.Getenv("BRANCHCODE"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func updateAdmission(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("admID"))
	branchCode := ps.ByName("branchcode")
	var adm Admission
	//dt, _ := ioutil.ReadAll(r.Body)
	//fmt.Println(string(dt))
	if err := json.NewDecoder(r.Body).Decode(&adm); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`update admission set date = $1, count = $2, edition_id = $3 where id = $4 and branch_code = $5;`, adm.Date, adm.Count, adm.EditionID, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func deleteAdmission(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("admID"))
	branchCode := ps.ByName("branchcode")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`delete from admission where id = $1 and branch_code = $2;`, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}
