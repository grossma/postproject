package http

import (
	"github.com/rs/cors"
	"net"
	"net/http"
	"os"
)

const (
	internalError = "internal error"
)

// Server represents an HTTP server.
type Server struct {
	ln net.Listener

	// Handler to serve.
	Handler http.Handler

	// Bind address to open.
	Addr string
}

// NewServer returns a new instance of Server.
func NewServer() *Server {
	return &Server{
		Addr: ":" + os.Getenv("PORT"),
	}
}

// Open opens a socket and serves the HTTP server.
func (s *Server) Open() error {
	// Open socket.
	ln, err := net.Listen("tcp", s.Addr)
	if err != nil {
		return err
	}
	s.ln = ln

	//cors
	handler := cors.AllowAll().Handler(s.Handler)

	// Start HTTP server.
	go func() { http.Serve(s.ln, handler) }()

	return nil
}

// Close closes the socket.
func (s *Server) Close() error {
	if s.ln != nil {
		s.ln.Close()
	}
	return nil
}

// Port returns the port that the server is open on. Only valid after open.
func (s *Server) Port() int {
	return s.ln.Addr().(*net.TCPAddr).Port
}
