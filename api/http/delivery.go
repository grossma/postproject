package http

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
)

type Delivery struct {
	ID             int    `json:"id"`
	SubscriptionID int    `json:"subscription_id"`
	Count          int    `json:"count"`
	EmployeeID     int    `json:"employee_id"`
	BranchCode     string `json:"branch_code"`
}

func getDelivery(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	rows, err := db.Query(`select id, subscription_id, count, employee_id, branch_code from delivery;`)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	delivery := make([]Delivery, 0)
	for rows.Next() {
		dlv := Delivery{}
		if err := rows.Scan(&dlv.ID, &dlv.SubscriptionID, &dlv.Count, &dlv.EmployeeID, &dlv.BranchCode); err != nil {
			logrus.Error(err.Error())
			continue
		}
		delivery = append(delivery, dlv)
	}
	if err := json.NewEncoder(w).Encode(delivery); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func createDelivery(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var dlv Delivery
	if err := json.NewDecoder(r.Body).Decode(&dlv); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err := db.Query(`insert into delivery (subscription_id, count, employee_id, branch_code) values ($1, $2, $3, $4);`, dlv.SubscriptionID, dlv.Count, dlv.EmployeeID, os.Getenv("BRANCHCODE"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func updateDelivery(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("dlvID"))
	branchCode := ps.ByName("branchcode")
	var dlv Delivery
	//dt, _ := ioutil.ReadAll(r.Body)
	//fmt.Println(string(dt))
	if err := json.NewDecoder(r.Body).Decode(&dlv); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`update delivery set subscription_id = $1, count = $2, employee_id = $3 where id = $4 and branch_code = $5;`, dlv.SubscriptionID, dlv.Count, dlv.EmployeeID, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func deleteDelivery(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("dlvID"))
	branchCode := ps.ByName("branchcode")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`delete from delivery where id = $1 and branch_code = $2;`, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}
