package http

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
)

type Edition struct {
	ID              int     `json:"id"`
	Name            string  `json:"name"`
	Periodicity     string  `json:"periodicity"`
	Price           float64 `json:"price"`
	PublishingHouse string  `json:"publishing_house"`
}

func getEdition(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	rows, err := db.Query(`select id, name, periodicity, price, publishing_house from edition;`)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	edition := make([]Edition, 0)
	for rows.Next() {
		edit := Edition{}
		if err := rows.Scan(&edit.ID, &edit.Name, &edit.Periodicity, &edit.Price, &edit.PublishingHouse); err != nil {
			logrus.Error(err.Error())
			continue
		}
		edition = append(edition, edit)
	}
	if err := json.NewEncoder(w).Encode(edition); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func createEdition(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	if os.Getenv("BRANCHCODE") != "mainoffice" {
		http.Error(w, "no rights", http.StatusForbidden)
		return
	}
	var edit Edition
	if err := json.NewDecoder(r.Body).Decode(&edit); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err := db.Query(`insert into edition (name, periodicity, price, publishing_house) values ($1, $2, $3, $4);`, edit.Name, edit.Periodicity, edit.Price, edit.PublishingHouse)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func updateEdition(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if os.Getenv("BRANCHCODE") != "mainoffice" {
		http.Error(w, "no rights", http.StatusForbidden)
		return
	}
	id, err := strconv.Atoi(ps.ByName("editID"))
	var edit Edition
	//dt, _ := ioutil.ReadAll(r.Body)
	//fmt.Println(string(dt))
	if err := json.NewDecoder(r.Body).Decode(&edit); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`update edition set name = $1, periodicity = $2, price = $3, publishing_house = $4 where id = $5;`, edit.Name, edit.Periodicity, edit.Price, edit.PublishingHouse, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func deleteEdition(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	if os.Getenv("BRANCHCODE") != "mainoffice" {
		http.Error(w, "no rights", http.StatusForbidden)
		return
	}
	id, err := strconv.Atoi(ps.ByName("editID"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`delete from edition where id = $1;`, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}
