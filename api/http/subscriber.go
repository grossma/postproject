package http

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
)

type Subscriber struct {
	ID         int    `json:"id"`
	FIO        string `json:"fio"`
	Address    string `json:"address"`
	Phone      string `json:"phone"`
	BranchCode string `json:"branch_code"`
}

func getSubscriber(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	rows, err := db.Query(`select id, fio, address, phone, branch_code from subscriber;`)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	subscriber := make([]Subscriber, 0)
	for rows.Next() {
		sub := Subscriber{}
		if err := rows.Scan(&sub.ID, &sub.FIO, &sub.Address, &sub.Phone, &sub.BranchCode); err != nil {
			logrus.Error(err.Error())
			continue
		}
		subscriber = append(subscriber, sub)
	}
	if err := json.NewEncoder(w).Encode(subscriber); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func createSubscriber(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var sub Subscriber
	if err := json.NewDecoder(r.Body).Decode(&sub); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err := db.Query(`insert into subscriber (fio, address, phone, branch_code) values ($1, $2, $3, $4);`, sub.FIO, sub.Address, sub.Phone, os.Getenv("BRANCHCODE"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func updateSubscriber(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("subID"))
	branchCode := ps.ByName("branchcode")
	var sub Subscriber
	//dt, _ := ioutil.ReadAll(r.Body)
	//fmt.Println(string(dt))
	if err := json.NewDecoder(r.Body).Decode(&sub); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`update subscriber set fio = $1, address = $2, phone = $3 where id = $4 and branch_code = $5;`, sub.FIO, sub.Address, sub.Phone, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func deleteSubscriber(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("subID"))
	branchCode := ps.ByName("branchcode")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`delete from subscriber where id = $1 and branch_code = $2;`, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}
