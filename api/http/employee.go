package http

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
)

type Employee struct {
	ID         int    `json:"id"`
	FIO        string `json:"fio"`
	BranchCode string `json:"branch_code"`
}

func getEmployee(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	rows, err := db.Query(`select id, fio, branch_code from employee;`)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	employee := make([]Employee, 0)
	for rows.Next() {
		emp := Employee{}
		if err := rows.Scan(&emp.ID, &emp.FIO, &emp.BranchCode); err != nil {
			logrus.Error(err.Error())
			continue
		}
		employee = append(employee, emp)
	}
	if err := json.NewEncoder(w).Encode(employee); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func createEmployee(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var emp Employee
	if err := json.NewDecoder(r.Body).Decode(&emp); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err := db.Query(`insert into employee (fio, branch_code) values ($1, $2);`, emp.FIO, os.Getenv("BRANCHCODE"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func updateEmployee(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("empID"))
	branchCode := ps.ByName("branchcode")
	var emp Employee
	//dt, _ := ioutil.ReadAll(r.Body)
	//fmt.Println(string(dt))
	if err := json.NewDecoder(r.Body).Decode(&emp); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`update employee set fio = $1  where id = $2 and branch_code = $3;`, emp.FIO, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func deleteEmployee(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("empID"))
	branchCode := ps.ByName("branchcode")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`delete from employee where id = $1 and branch_code = $2;`, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}
