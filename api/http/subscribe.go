package http

import (
	"encoding/json"
	"github.com/julienschmidt/httprouter"
	"github.com/sirupsen/logrus"
	"net/http"
	"os"
	"strconv"
	"time"
)

type Subscription struct {
	ID           int       `json:"id"`
	SubscriberID int       `json:"subscriber_id"`
	Date         time.Time `json:"date"`
	Price        float64   `json:"price"`
	Count        int       `json:"count"`
	EditionID    int       `json:"edition_id"`
	BranchCode   string    `json:"branch_code"`
}

func getSubscribe(w http.ResponseWriter, _ *http.Request, _ httprouter.Params) {
	rows, err := db.Query(`select id, date, price, count, subscriber_id, edition_id, branch_code from subscribe;`)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	subscribtions := make([]Subscription, 0)
	for rows.Next() {
		sub := Subscription{}
		if err := rows.Scan(&sub.ID, &sub.Date, &sub.Price, &sub.Count, &sub.SubscriberID, &sub.EditionID, &sub.BranchCode); err != nil {
			logrus.Error(err.Error())
			continue
		}
		subscribtions = append(subscribtions, sub)
	}
	if err := json.NewEncoder(w).Encode(subscribtions); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func createSubscribe(w http.ResponseWriter, r *http.Request, _ httprouter.Params) {
	var sub Subscription
	if err := json.NewDecoder(r.Body).Decode(&sub); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err := db.Query(`insert into subscribe (date, price, count, subscriber_id, edition_id, branch_code) values ($1, $2, $3, $4, $5, $6);`, sub.Date, sub.Price, sub.Count, sub.SubscriberID, sub.EditionID, os.Getenv("BRANCHCODE"))
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func updateSubscribe(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("subID"))
	branchCode := ps.ByName("branchcode")
	var sub Subscription
	//dt, _ := ioutil.ReadAll(r.Body)
	//fmt.Println(string(dt))
	if err := json.NewDecoder(r.Body).Decode(&sub); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`update subscribe set date = $1, Price = $2, count = $3, subscriber_id = $4, edition_id = $5 where id = $6 and branch_code = $7;`, sub.Date, sub.Price, sub.Count, sub.SubscriberID, sub.EditionID, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}

func deleteSubscribe(w http.ResponseWriter, r *http.Request, ps httprouter.Params) {
	id, err := strconv.Atoi(ps.ByName("subID"))
	branchCode := ps.ByName("branchcode")
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	_, err = db.Query(`delete from subscribe where id = $1 and branch_code = $2;`, id, branchCode)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	if err := json.NewEncoder(w).Encode("ok"); err != nil {
		logrus.Errorf("can't answer: %s", err.Error())
	}
}
