package postgres

import (
	"fmt"
	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
	"github.com/sirupsen/logrus"
	"os"
	"os/exec"
)

var (
	host     = os.Getenv("db_host")
	port     = os.Getenv("db_port")
	name     = os.Getenv("db_name")
	password = os.Getenv("db_password")
	user     = os.Getenv("db_user")
)

type PgStore struct {
	Db *sqlx.DB
}

func (pg *PgStore) Open() error {
	psqlInfo := fmt.Sprintf("host=%s port=%s user=%s password=%s dbname=%s sslmode=disable", host, port, user, password, name)
	db, err := sqlx.Open("postgres", psqlInfo)
	if err != nil {
		return err
	}
	runSubscriptions()
	runSeeds()
	pg.Db = db
	return nil
}

func (pg *PgStore) Close() {
	if err := pg.Db.Close(); err != nil {
		logrus.Errorf("can't close db connection")
	}
}

func runSubscriptions() {
	//query, err := ioutil.ReadFile("/etc/create-subscriptions.sql")
	//if err != nil {
	//	logrus.Error(err)
	//	return
	//}
	if _, err := exec.Command(fmt.Sprintf("psql -U %s %s < /etc/create-subscriptions.sql", os.Getenv("db_user"), os.Getenv("db_name"))).Output(); err != nil {
		logrus.Error(err)
		return
	}

}

func runSeeds() {
	//query, err := ioutil.ReadFile("/etc/insert_data.sql")
	//if err != nil {
	//	logrus.Error(err)
	//	return
	//}
	//if _, err := db.Exec(string(query)); err != nil {
	//	logrus.Error(err)
	//	return
	//}
	if _, err := exec.Command(fmt.Sprintf("psql -U %s %s < /etc/insert_data.sql", os.Getenv("db_user"), os.Getenv("db_name"))).Output(); err != nil {
		logrus.Error(err)
		return
	}
}
