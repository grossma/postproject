import React, { Component } from 'react'
import ReactDOM from 'react-dom';
import { Container, Row, Col } from 'reactstrap'
// import DataTable from './Components/Tables/DataTable'
import App from './App'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';

import Constants from './stdafx'

class Signin extends React.Component {

    constructor(props) {
        super(props);
        this.handleLoginChange = this.handleLoginChange.bind(this);
        this.handlePasswordChange = this.handlePasswordChange.bind(this);
        this.signIn = this.signIn.bind(this);
        this.state = {
          login:'',
          password:''
        };
    }

    signIn(){
        fetch(Constants.Host() + '/auth/login', {
            method: 'post',
            headers: {
              'Content-Type': 'application/json'
            },
            body: JSON.stringify({
              login: this.state.login,
              password: this.state.password,
            })
          })
            .then(response => response.json())
            .then(item => {
              alert(item)
              if(item) {
                ReactDOM.render(<App />, document.getElementById('root'));    
              } else {
                console.log('failure')
              }
            })
            .catch(err => console.log(err))
        //alert('Login is ' + this.state.login + ' Password is ' + this.state.password);  
        if(this.state.login == "admin" && this.state.password == "admin"){
          alert('admin')
          ReactDOM.render(<App mode = "admin"/>, document.getElementById('root'));    
        } else if(this.state.login == "user" && this.state.password == "user"){
            alert('user')
            ReactDOM.render(<App mode = "user"/>, document.getElementById('root'));
        } else{
          alert("fail");
        }

    }

    handleLoginChange(e){
        this.setState({login:e.target.value})
    }
    handlePasswordChange(e){
        this.setState({password:e.target.value})
    }

    render() {
        return (
        <form className="form-signin">
                <h2 className="form-signin-heading"> Please sign in </h2>
                <label for="inputLogin" className="sr-only"> Login address</label>
                <input type="email" onChange={this.handleLoginChange} id="inputLogin" className="form-control" placeholder="Login address" required autofocus />
                <label for="inputPassword" className="sr-only"> Password</label>
                <input type="password" onChange={this.handlePasswordChange} id="inputPassword" className="form-control" placeholder="Password" required />
                <button className="btn btn-lg btn-primary btn-block" onClick={this.signIn} type="button"> Sign in</button>
            </form>
        )
    }
} 
export default Signin  