import React, { Component } from 'react'
import { Table, Button } from 'reactstrap';
import ModalForm from '../Modals/ModalDelivery'

import Constants from '../../stdafx'

class Delivery_Dt extends Component {

  deleteItem = (id, branch_code) => {
    let confirmDelete = window.confirm('Delete item forever?')
    if(confirmDelete){
      fetch(Constants.Host() + '/delivery/'+ branch_code + "/" + id + '/', {
      method: 'delete',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        this.props.deleteItemFromState(id)
      })
      .catch(err => console.log(err))
    }

  }

  render() {

console.log(this.props.items)

    const items = this.props.items.map(item => {
      return (
        <tr key={item.id}>
          <th scope="row">{item.id}</th>
          <td>{item.subscription_id}</td>
          <td>{item.count}</td>
          <td>{item.employee_id}</td>
		  <td>{item.branch_code}</td>
          <td>
            <div style={{width:"110px"}}>
              <ModalForm buttonLabel="Edit" item={item} updateState={this.props.updateState}/>
              {' '}
              <Button color="danger" onClick={() => this.deleteItem(item.id, item.branch_code)}>Del</Button>
            </div>
          </td>
        </tr>
        )
      })

    return (
      <Table responsive hover>
        <thead>
          <tr>
            <th>ID</th>
            <th>Subscription ID</th>
            <th>Count</th>
            <th>Employee ID</th>
			<th>Branch code</th>
          </tr>
        </thead>
        <tbody>
          {items}
        </tbody>
      </Table>
    )
  }
}

export default Delivery_Dt