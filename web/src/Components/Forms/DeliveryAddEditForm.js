import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Constants from '../../stdafx'

class AddEditForm extends React.Component {


  state = {
    id: 0,
    subscription_id: '',
	count: '',
    employee_id: '',
    branch_code: '',
  }

  onChange = e => {
    this.setState({[e.target.name]: e.target.value})
  }

  submitFormAdd = e => {
    e.preventDefault()
    fetch(Constants.Host() + '/delivery/', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        subscription_id: parseFloat(this.state.subscription_id),
        count: parseFloat(this.state.count),
        employee_id: parseFloat(this.state.employee_id),
		branch_code: this.state.branch_code
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        if(Array.isArray(item)) {
          this.props.addItemToState(item[0])
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  submitFormEdit = e => {
    e.preventDefault()
    fetch(Constants.Host() + '/delivery/'+ this.state.branch_code + "/" + this.state.id, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.state.id,
        subscription_id: parseFloat(this.state.subscription_id),
        count: this.state.count,
        employee_id: parseFloat(this.state.employee_id),
		branch_code: this.state.branch_code
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        if(Array.isArray(item)) {
          // console.log(item[0])
          this.props.updateState(item[0])
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  componentDidMount(){
    // if item exists, populate the state with proper data
    if(this.props.item){
      const { id, subscription_id, count, employee_id, branch_code} = this.props.item
      this.setState({ id, subscription_id, count, employee_id, branch_code})
    }
  }

  render() {
    return (
      <Form onSubmit={this.props.item ? this.submitFormEdit : this.submitFormAdd}>
        <FormGroup>
          <Label for="subscription_id">Ид подписки</Label>
          <Input type="number" name="subscription_id" id="subscription_id" onChange={this.onChange} value={this.state.subscription_id === null ? '' : this.state.subscription_id} />
        </FormGroup>
        <FormGroup>
          <Label for="count">Количество</Label>
          <Input type="number" name="count" id="count" onChange={this.onChange} value={this.state.count === null ? '' : this.state.count}/>
        </FormGroup>
		<FormGroup>
          <Label for="employee_id">Ид сотрудника</Label>
          <Input type="number" name="employee_id" id="employee_id" onChange={this.onChange} value={this.state.employee_id === null ? '' : this.state.employee_id}/>
        </FormGroup>
		<FormGroup>
          <Label for="branch_code">Код отделения</Label>
          <Input type="text" name="branch_code" id="branch_code" onChange={this.onChange} value={this.state.branch_code === null ? '' : this.state.branch_code}/>
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default AddEditForm