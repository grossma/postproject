import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Constants from '../../stdafx'

class AddEditForm extends React.Component {

  state = {
    id: 0,
    subscriber_id: '',
	date: '',
    count: '',
	edition_id: '',
	branch_code: ''
  }

  onChange = e => {
    this.setState({[e.target.name]: e.target.value})
  }

  submitFormAdd = e => {
    e.preventDefault()
    fetch(Constants.Host() + '/subscribe/', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        subscriber_id: parseFloat(this.state.subscriber_id),
        date: this.state.date,
        count: parseFloat(this.state.count),
		edition_id: parseFloat(this.state.edition_id),
		branch_code: this.state.branch_code
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        if(Array.isArray(item)) {
          this.props.addItemToState(item[0])
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  submitFormEdit = e => {
    e.preventDefault()
    fetch(Constants.Host() + '/subscribe/'+ this.state.branch_code + "/" + this.state.id, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.state.id,
        subscriber_id: parseFloat(this.state.subscriber_id),
        date: this.state.date,
        count: parseFloat(this.state.count),
		edition_id: parseFloat(this.state.edition_id),
		branch_code: this.state.branch_code
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        if(Array.isArray(item)) {
          // console.log(item[0])
          this.props.updateState(item[0])
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  componentDidMount(){
    // if item exists, populate the state with proper data
    if(this.props.item){
      const { id, subscriber_id, date, count, edition_id, branch_code} = this.props.item
      this.setState({ id, subscriber_id, date, count, edition_id, branch_code})
    }
  }

  render() {
    return (
      <Form onSubmit={this.props.item ? this.submitFormEdit : this.submitFormAdd}>
        <FormGroup>
          <Label for="subscriber_id">Ид подписчика</Label>
          <Input type="number" name="subscriber_id" id="subscriber_id" onChange={this.onChange} value={this.state.subscriber_id === null ? '' : this.state.subscriber_id} />
        </FormGroup>
        <FormGroup>
          <Label for="date">Дата</Label>
          <Input type="text" name="date" id="date" onChange={this.onChange} value={this.state.date === null ? '' : this.state.date}/>
        </FormGroup>
        <FormGroup>
          <Label for="count">Количество</Label>
          <Input type="number" name="count" id="count" onChange={this.onChange} value={this.state.count === null ? '' : this.state.count}/>
        </FormGroup>
		<FormGroup>
          <Label for="edition_id">Ид издания</Label>
          <Input type="number" name="edition_id" id="edition_id" onChange={this.onChange} value={this.state.edition_id === null ? '' : this.state.edition_id}/>
        </FormGroup>
		<FormGroup>
          <Label for="branch_code">Код отделения</Label>
          <Input type="text" name="branch_code" id="branch_code" onChange={this.onChange} value={this.state.branch_code === null ? '' : this.state.branch_code}/>
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default AddEditForm