import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Constants from '../../stdafx'

class AddEditForm extends React.Component {

  state = {
    id: 0,
    fio: '',
    branch_code: ''
  }

  onChange = e => {
    this.setState({[e.target.name]: e.target.value})
  }

  submitFormAdd = e => {
    e.preventDefault()
    fetch(Constants.Host() + '/employee/', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        fio: this.state.fio,
        branch_code: this.state.branch_code
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        if(Array.isArray(item)) {
          this.props.addItemToState(item[0])
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  submitFormEdit = e => {
    e.preventDefault()
    fetch(Constants.Host() + '/employee/'+ this.state.branch_code + "/" + this.state.id, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.state.id,
        fio: this.state.fio,
        branch_code: this.state.branch_code
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        if(Array.isArray(item)) {
          // console.log(item[0])
          this.props.updateState(item[0])
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  componentDidMount(){
    // if item exists, populate the state with proper data
    if(this.props.item){
      const { id, fio, branch_code} = this.props.item
      this.setState({ id, fio, branch_code})
    }
  }

  render() {
    return (
      <Form onSubmit={this.props.item ? this.submitFormEdit : this.submitFormAdd}>
        <FormGroup>
          <Label for="fio">ФИО</Label>
          <Input type="text" name="fio" id="fio" onChange={this.onChange} value={this.state.fio === null ? '' : this.state.fio} />
        </FormGroup>
        <FormGroup>
          <Label for="branch_code">Код отделения</Label>
          <Input type="text" name="branch_code" id="branch_code" onChange={this.onChange} value={this.state.branch_code === null ? '' : this.state.branch_code}/>
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default AddEditForm