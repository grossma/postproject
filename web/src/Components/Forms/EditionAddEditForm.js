import React from 'react';
import { Button, Form, FormGroup, Label, Input } from 'reactstrap';
import Constants from '../../stdafx'


class AddEditForm extends React.Component {

  state = {
    id: 0,
    periodicity: '',
    name:'',
    price: '',
    publishing_house: '',
  }

  onChange = e => {
    this.setState({[e.target.name]: e.target.value})
  }

  submitFormAdd = e => {
    e.preventDefault()
    fetch(Constants.Host() + '/edition/', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        periodicity: this.state.periodicity,
        name: this.state.name,
        price: parseFloat(this.state.price),
        publishing_house: this.state.publishing_house
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        if(Array.isArray(item)) {
          this.props.addItemToState(item[0])
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  submitFormEdit = e => {
    e.preventDefault()
    fetch(Constants.Host() + '/edition/'+ this.state.id, {
      method: 'put',
      headers: {
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({
        id: this.state.id,
        periodicity: this.state.periodicity,
        name: this.state.name,
        price: parseFloat(this.state.price),
        publishing_house: this.state.publishing_house
      })
    })
      .then(response => {
        if(response.status == 403){
            alert("Action is forbidden")
        }
        if(response.status == 500){
          response.text().then(text => alert(text))
        }
        response.json()
      })
      .then(item => {
        if(Array.isArray(item)) {
          // console.log(item[0])
          this.props.updateState(item[0])
          this.props.toggle()
        } else {
          console.log('failure')
        }
      })
      .catch(err => console.log(err))
  }

  componentDidMount(){
    // if item exists, populate the state with proper data
    if(this.props.item){
      const { id, periodicity, name, price, publishing_house} = this.props.item
      this.setState({ id, periodicity, name, price, publishing_house})
    }
  }

  render() {
    return (
      <Form onSubmit={this.props.item ? this.submitFormEdit : this.submitFormAdd}>
        <FormGroup>
          <Label for="periodicity">Периодичность</Label>
          <Input type="text" name="periodicity" id="periodicity" onChange={this.onChange} value={this.state.periodicity === null ? '' : this.state.periodicity} />
        </FormGroup>
        <FormGroup>
          <Label for="name">Название</Label>
          <Input type="text" name="name" id="name" onChange={this.onChange} value={this.state.name === null ? '' : this.state.name} />
        </FormGroup>
        <FormGroup>
          <Label for="price">Цена</Label>
          <Input type="number" name="price" id="price" onChange={this.onChange} value={this.state.price === null ? '' : this.state.price} />
        </FormGroup>
        <FormGroup>
          <Label for="publishing_house">Название издания</Label>
          <Input type="text" name="publishing_house" id="publishing_house" onChange={this.onChange} value={this.state.publishing_house === null ? '' : this.state.publishing_house}/>
        </FormGroup>
        <Button>Submit</Button>
      </Form>
    );
  }
}

export default AddEditForm