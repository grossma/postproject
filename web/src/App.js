import React, { Component } from 'react'
import { Container, Row, Col } from 'reactstrap'
// import DataTable from './Components/Tables/DataTable'
import { BrowserRouter as Router, Switch, Route, Link } from 'react-router-dom';
import MainPage_Subscribers from './Components/MainPages/MainPage_Subscribers'
import MainPage_Subscribes from './Components/MainPages/MainPage_Subscribes'
import MainPage_Editions from './Components/MainPages/MainPage_Editions'
import MainPage_Admissions from './Components/MainPages/MainPage_Admissions'
import MainPage_Deliveries from './Components/MainPages/MainPage_Deliveries'
import MainPage_Employees from './Components/MainPages/MainPage_Employees'

class App extends Component {
  state = {
    items: []
  }

  getItems(){
    fetch('http://localhost:3000/crud')
      .then(response => response.json())
      .then(items => this.setState({items}))
      .catch(err => console.log(err))
  }

  addItemToState = (item) => {
    this.setState(prevState => ({
      items: [...prevState.items, item]
    }))
  }

  updateState = (item) => {
    const itemIndex = this.state.items.findIndex(data => data.id === item.id)
    const newArray = [
    // destructure all items from beginning to the indexed item
      ...this.state.items.slice(0, itemIndex),
    // add the updated item to the array
      item,
    // add the rest of the items to the array from the index after the replaced item
      ...this.state.items.slice(itemIndex + 1)
    ]
    this.setState({ items: newArray })
  }

  deleteItemFromState = (id) => {
    const updatedItems = this.state.items.filter(item => item.id !== id)
    this.setState({ items: updatedItems })
  }

  componentDidMount(){
    this.getItems()
  }

  tableList = [
    {name:"Подписчики", path: "/subscribers"},
    {name:"Подписки", path:"/subscribes"},
    {name:"Издания", path:"/edition"},
    {name:"Поставки", path:"/admission"},
    {name:"Доставки", path:"/delivery"},
    {name:"Сотртудники", path:"/employee"}
  ];

  render() {
    console.log("РЕНДЕРИМ ГЛАВНУЮ")

    if(this.props.mode == "admin")    {
      this.tableList = [
        {name:"Подписчики", path: "/subscribers"},
        {name:"Подписки", path:"/subscribes"},
        {name:"Издания", path:"/edition"},
        {name:"Поставки", path:"/admission"},
        {name:"Доставки", path:"/delivery"},
        {name:"Сотртудники", path:"/employee"}
      ];
    }
    if(this.props.mode == "user"){
      this.tableList = [
        {name:"Подписчики", path: "/subscribers"},
        {name:"Подписки", path:"/subscribes"},
        {name:"Издания", path:"/edition"},
        {name:"Поставки", path:"/admission"},
        {name:"Доставки", path:"/delivery"},
      ];
    }

    return (
      <Container className="App">
        <Row>
          <Col>
            <h1 style={{margin: "20px 0"}}>СУБД</h1>
          </Col>
        </Row>
        <Row>
          <Router>
            <Col-3>
            {
              this.tableList.map(({name, path})=>(
                <Row>
                  <Col-9>
                    <Link to = {path}>
                      <button type="button" class="btn btn-primary">{name}</button>
                    </Link>
                  </Col-9>
                </Row>
              ))
            }
            </Col-3>
            <Switch>
              <Route exact path ='/subscribers'><Col><MainPage_Subscribers/></Col></Route>
              <Route exact path ='/subscribes'><Col><MainPage_Subscribes/></Col></Route>
              <Route exact path ='/edition'><Col><MainPage_Editions/></Col></Route>
              <Route exact path ='/admission'><Col><MainPage_Admissions/></Col></Route>
              <Route exact path ='/delivery'><Col><MainPage_Deliveries/></Col></Route>
              <Route exact path ='/employee'><Col><MainPage_Employees/></Col></Route>
            </Switch>
          </Router>
        </Row>
      </Container>
    )
  }
}

export default App